# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_08_09_190036) do

  create_table "activities", force: :cascade do |t|
    t.string "description"
    t.integer "lesson_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["lesson_id"], name: "index_activities_on_lesson_id"
  end

  create_table "collaborations", force: :cascade do |t|
    t.integer "kind"
    t.integer "user_id"
    t.integer "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_collaborations_on_course_id"
    t.index ["user_id"], name: "index_collaborations_on_user_id"
  end

  create_table "courses", force: :cascade do |t|
    t.string "name"
    t.text "classPlan"
    t.string "environmentSetting"
    t.integer "selective_process_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["selective_process_id"], name: "index_courses_on_selective_process_id"
  end

  create_table "feedbacks", force: :cascade do |t|
    t.string "content"
    t.integer "user_id"
    t.string "feedbackable_type"
    t.integer "feedbackable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["feedbackable_type", "feedbackable_id"], name: "index_feedbacks_on_feedbackable_type_and_feedbackable_id"
    t.index ["user_id"], name: "index_feedbacks_on_user_id"
  end

  create_table "groups", force: :cascade do |t|
    t.integer "activity_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["activity_id"], name: "index_groups_on_activity_id"
  end

  create_table "groups_users", force: :cascade do |t|
    t.integer "group_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_groups_users_on_group_id"
    t.index ["user_id"], name: "index_groups_users_on_user_id"
  end

  create_table "lessons", force: :cascade do |t|
    t.date "day"
    t.datetime "startTime"
    t.time "duration"
    t.string "subject"
    t.text "description"
    t.integer "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_lessons_on_course_id"
  end

  create_table "lessons_subjects", force: :cascade do |t|
    t.integer "subject_id"
    t.integer "lesson_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["lesson_id"], name: "index_lessons_subjects_on_lesson_id"
    t.index ["subject_id"], name: "index_lessons_subjects_on_subject_id"
  end

  create_table "materials", force: :cascade do |t|
    t.text "description"
    t.string "link"
    t.integer "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_materials_on_course_id"
  end

  create_table "participations", force: :cascade do |t|
    t.integer "situation"
    t.integer "user_id"
    t.integer "selective_process_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["selective_process_id"], name: "index_participations_on_selective_process_id"
    t.index ["user_id"], name: "index_participations_on_user_id"
  end

  create_table "presences", force: :cascade do |t|
    t.integer "status"
    t.integer "performance"
    t.text "observation"
    t.time "minutes_late"
    t.integer "user_id"
    t.integer "lesson_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["lesson_id"], name: "index_presences_on_lesson_id"
    t.index ["user_id"], name: "index_presences_on_user_id"
  end

  create_table "selective_processes", force: :cascade do |t|
    t.string "year"
    t.integer "schoolTerm"
    t.date "startDate"
    t.date "registrationDeadline"
    t.date "finishDate"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "subjects", force: :cascade do |t|
    t.string "subject"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "submissions", force: :cascade do |t|
    t.integer "status"
    t.integer "performance"
    t.string "link"
    t.text "observation"
    t.string "submissable_type"
    t.integer "submissable_id"
    t.integer "activity_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["activity_id"], name: "index_submissions_on_activity_id"
    t.index ["submissable_type", "submissable_id"], name: "index_submissions_on_submissable_type_and_submissable_id"
  end

  create_table "users", force: :cascade do |t|
    t.boolean "adm"
    t.string "name"
    t.integer "kind"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email"
    t.string "password_digest"
  end

end
