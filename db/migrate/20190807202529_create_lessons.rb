class CreateLessons < ActiveRecord::Migration[5.2]
  def change
    create_table :lessons do |t|
      t.date :day
      t.timestamp :startTime
      t.time :duration
      t.string :subject
      t.text :description
      t.references :course, foreign_key: true

      t.timestamps
    end
  end
end
