class CreateParticipations < ActiveRecord::Migration[5.2]
  def change
    create_table :participations do |t|
      t.integer :situation
      t.references :user, foreign_key: true
      t.references :selective_process, foreign_key: true

      t.timestamps
    end
  end
end
