class CreateLessonsSubjects < ActiveRecord::Migration[5.2]
  def change
    create_table :lessons_subjects do |t|
      t.references :subject, foreign_key: true
      t.references :lesson, foreign_key: true

      t.timestamps
    end
  end
end
