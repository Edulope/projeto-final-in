class CreateFeedbacks < ActiveRecord::Migration[5.2]
  def change
    create_table :feedbacks do |t|
      t.string :content
      t.references :user, foreign_key: true
      t.references :feedbackable, polymorphic: true

      t.timestamps
    end
  end
end
