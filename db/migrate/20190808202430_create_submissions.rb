class CreateSubmissions < ActiveRecord::Migration[5.2]
  def change
    create_table :submissions do |t|
      t.integer :status
      t.integer :performance
      t.string :link
      t.text :observation
      t.references :submissable, polymorphic: true
      t.references :activity, foreign_key: true

      t.timestamps
    end
  end
end
