# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
SelectiveProcess.create(year:"2019.2", schoolTerm: 2, startDate: "04/06/2019", registrationDeadline: "20/06/2019", finishDate: "2019-08-07");
Course.create(name:"rails", classPlan:"demonstrar as principais utilidades de rails", environmentSetting:"laboratorio", selective_process_id: 1);
Material.create(description: "aprendendo a usar rails", link: "https://guides.rubyonrails.org/association_basics.html", course_id: 1);
Lesson.create(day: "05/06/2019", startTime: Time.zone.now, duration: "02:00:00", subject:"rails parte 2", description:"mais rails...", course_id:1);
Subject.create(subject:"iniciando projeto", description:"ensinando a usar rails new");
s1 = Subject.all[0];
s1.lessons<<Lesson.first;
Activity.create(description: "faça uma calculadora em wordpress(sim, eu sei que a aula é de rails)", lesson_id: 1);
User.create(adm: false, name:"joao pedro", email:"jp-dealmeida@hotmail.com", password:"abcd1234", kind: 1);
User.create(adm: true, name:"joao", email:"almeidajoao@id.uff.br", password:"abcd1234", kind: 1);
Participation.create(selective_process_id: 1, user_id: 1, situation: 1);
SelectiveProcess.create(year:"2019.1", schoolTerm: 1, startDate: "04/01/2019", registrationDeadline: "20/01/2019", finishDate: "2019-03-07");
Collaboration.create(course_id: 1, user_id: 1, kind: 1);
Presence.create(status: 1 , performance: 1, observation:"mandei bem", minutes_late: "02:00:00", user_id:1, lesson_id:1);
s1 = Lesson.first;
Feedback.create(user_id: 1, feedbackable: s1, content:"aula boa");
s1 = User.first;
Submission.create(activity_id: 1, submissable: s1, status: 1, link: "link do feedback", performance: 1, observation: "enviei o trabalho, pronto");
s1 = Submission.first;
Feedback.create(user_id: 1, feedbackable: s1, content:"seu trabalho ta ruinzao kkkkk");
s1 = Group.create(activity_id:1)
Submission.create(activity_id: 1, submissable: s1, status: 1, link: "aeeee", performance: 1, observation: "NOSSO TRABALHO EM GRUPO TA FEITO PROFESSOR");
s1 = User.all[0];
s1.groups<<Group.first;
Lesson.create(day: "05/06/2019", startTime: "2019-08-09T13:39:50.762Z", duration: "02:00:00", subject:"rails parte 2", description:"mais rails...", course_id:1);