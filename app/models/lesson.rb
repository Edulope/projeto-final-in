class Lesson < ApplicationRecord
  belongs_to :course
  has_and_belongs_to_many :subjects
  has_many :activities
  has_many :presences
  has_many :feedbacks, as: :feedbackable
end
