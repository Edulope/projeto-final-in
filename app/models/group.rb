class Group < ApplicationRecord
  belongs_to :activity
  has_many :submissions, as: :submissable
  has_and_belongs_to_many :users
end
