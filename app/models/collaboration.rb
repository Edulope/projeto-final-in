class Collaboration < ApplicationRecord
  enum kind: [:cursando, :aprovado, :reprovado, :desistente, :desqualificado]
  belongs_to :user
  belongs_to :course
end
