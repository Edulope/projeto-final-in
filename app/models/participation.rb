class Participation < ApplicationRecord
  enum situation: [:cursando, :aprovado, :reprovado, :desistente, :desqualificado]
  belongs_to :user
  belongs_to :selective_process
end
