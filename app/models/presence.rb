class Presence < ApplicationRecord
  enum status: [:cursando, :aprovado, :reprovado, :desistente, :desqualificado]
  belongs_to :user
  belongs_to :lesson
end
