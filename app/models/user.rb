class User < ApplicationRecord
    has_secure_password
  validates :email, presence: true, uniqueness: true
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :password,
            length: { minimum: 6 },
            if: -> { new_record? || !password.nil? }


    enum kind: [:membro, :diretor, :exmembro, :candidato, :colaboradorExterno]
    has_many :participations
    has_many :collaborations
    has_many :presences
    has_many :feedbacks
    has_many :submissions, as: :submissable
    has_and_belongs_to_many :groups
    #has_many :selective_processes, through: :participations

end
