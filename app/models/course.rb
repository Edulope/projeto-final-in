class Course < ApplicationRecord
  belongs_to :selective_process
  has_many :materials
  has_many :lessons
  has_many :collaborations
end
