class Submission < ApplicationRecord
  belongs_to :submissable, polymorphic: true
  belongs_to :activity
  has_many :feedbacks, as: :feedbackable
end
