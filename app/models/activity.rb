class Activity < ApplicationRecord
  belongs_to :lesson
  has_many :submissions
end
