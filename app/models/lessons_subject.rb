class LessonsSubject < ApplicationRecord
  belongs_to :subject
  belongs_to :lesson
end
