class SubjectSerializer < ActiveModel::Serializer
  attributes :id, :subject, :description
  has_many :lessons
end
