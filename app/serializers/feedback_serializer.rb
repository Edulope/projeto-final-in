class FeedbackSerializer < ActiveModel::Serializer
  attributes :id, :content, :feedbackable_type
end
