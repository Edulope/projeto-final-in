class SubmissionSerializer < ActiveModel::Serializer
  attributes :id, :status, :performance, :link, :observation, :submissable_type
  has_one :submissable
  has_one :activity
  has_many :feedbacks, as: :feedbackable
end
