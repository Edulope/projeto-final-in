class CourseSerializer < ActiveModel::Serializer
  attributes :id, :name, :classPlan, :environmentSetting
  has_one :selective_process
  has_many :materials
  has_many :lessons
  has_many :collaborations
end
