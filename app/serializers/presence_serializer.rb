class PresenceSerializer < ActiveModel::Serializer
  attributes :id, :status, :performance, :observation, :minutes_late
end
