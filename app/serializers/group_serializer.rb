class GroupSerializer < ActiveModel::Serializer
  attributes :id
  has_one :activity
  has_many :submissions, as: :submissable
  has_many :users
end
