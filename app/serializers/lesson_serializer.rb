class LessonSerializer < ActiveModel::Serializer
  attributes :id, :day, :startTime, :duration, :subject, :description
  has_one :course
  has_many :subjects
  has_many :activities
  has_many :presences
  has_many :feedbacks, as: :feedbackable
end
