class SelectiveProcessSerializer < ActiveModel::Serializer
  attributes :id, :year, :schoolTerm, :startDate, :registrationDeadline, :finishDate
  has_many :courses
  #has_many :users, through: :participations
  has_many :participations
end
