class UserSerializer < ActiveModel::Serializer
  attributes :id, :adm, :name, :email, :kind
  has_many :participations
  has_many :collaborations
  has_many :presences
  has_many :feedbacks
  has_many :submissions, as: :submissable
  has_many :groups
end
