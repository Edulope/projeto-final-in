class ActivitySerializer < ActiveModel::Serializer
  attributes :id, :description
  has_one :lesson
  has_many :submissions
end
