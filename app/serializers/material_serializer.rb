class MaterialSerializer < ActiveModel::Serializer
  attributes :id, :description, :link
  has_one :course
end
