Rails.application.routes.draw do
  resources :groups
  resources :submissions
  resources :activities
  resources :subjects
  resources :lessons
  resources :materials
  resources :courses
  resources :selective_processes
  resources :feedbacks
  resources :presences
  resources :participations
  resources :collaborations
  resources :users, param: :_id
  post '/auth/login', to: 'authentication#login'
  get '/*a', to: 'application#not_found'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
